﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesMonstersInterface3.Models
{
    public class Foret
    {
        //Variables de champs privé :
        //Nombre d'Ennemis
        //Liste de monstre vivants
        //Liste de monstres morts
        //Bool pour la fin d'un combat
        //Bool pour la fin de la partie
        //NombreCombat
        //Un héro (Pour stocker notre héro)
        //Un Monstre (pour stocker le prochain monstre à attaquer)


        //Prop : 
        //NbCombat
        //EndGame
        //Hero
        //Monstre

        //Constructeur qui prend en paramètre le nom du héro (Humain ou Nain)
        //Dedans on initialise une nouvelle Liste de Monstre (vivants)
        //On initialise une nouvelle liste de Monstre (morts)
        
        //Pour i de 0 à NbEnnemis
        //A partir d'un dé 3, on fait un nouveau Loup si 1, Orque si 2, Dragonnet si 3
        //On ajoute la méthode PersonnageMort dans son action Meurt (voir méthode plus bas)
        //On l'ajoute dans la liste

        //On initialise la prop Hero avec...
        //Si le nom passé en param était Humain, on fait un humain, sinon, un nain
        //On lui ajoute la méthode PersonnageMort dans son action Meurt (voir méthode plus bas)
        //Fin constructeur

        //Méthode PersonnageMort, renvoie rien, prend en param un personnage
        //On passe la prop fin du combat à vrai
        //On enlève la méthode PersonnageMort du personnage passé en param
        //Si le personnage était un héro
            //Fin de la partie = true
            //Avec des Console write, on fait un bilan de fin de partie "Le héro est mort après X combats, il avait X Or, X cuir" ce que vous voulez :)
        //Si le personne était un Monstre
            //On affiche le monstre est mort
            //On augmente de 1 le nombre de combat
            //On fait se reposer notre héro
            //On fait dépouiller le monstre par notre héro
            //On ajoute le monstre à la liste des morts et on le tire à la liste des vivants
        //Si le tableau de monstre vivants est vide -> Fin de la partie

        //Méthod Start pour lancer le jeu, ne renvoie rien, pas de param
        //On fait un boolean tourDuHero à true (il commence toujours)
        //Tant que pas la fin du jeu
            //On attaque le premier monstre de la liste
            //Petit texte : On rencontre un monstre + ses stats
            //On met le bool FinDuCombat à faux
            //Tant que pas la fin du Combat
                //Si c'est le tour du héro
                    //Le héro frappe le monstre
                    //Le tour du héro passe à faux
                //Si c'est le tour du monstre
                    //Le monstre frappe le héro
                    //Le tour du héro passe à true
    }
}
