﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesMonstersInterface3.Models
{
    public class De
    {
        Random _Rand;

        public int Minimum { get { return 1; } private set { Minimum = value; } }
        public int Maximum { get; private set; }

        public De(int Max)
        {
            Maximum = Max;
            _Rand = new Random();
        }

        public int Lance()
        {
            return _Rand.Next(Minimum, Maximum + 1);
        }
        
        public static int CalculCarac()
        {
            int [] Des = new int[4];
            De De6 = new De(6);
            for(int i= 0; i < Des.Length; i++)
            {
                Des[i] = De6.Lance();
                //Console.WriteLine(Des[i]);
            }
            
            Array.Sort(Des);

            //Console.WriteLine("Après tri");
            //for (int i = 0; i < Des.Length; i++)
            //{
            //    Console.WriteLine(Des[i]);
            //}
            return Des[1] + Des[2] + Des[3];
        }
    }
}
