﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesMonstersInterface3.Models
{
    //Abstract aussi, on ne veut pas pouvoir faire d'instance de Hero, juste Humain ou Nain
    //Devra hérité de Personnage et implémenter l'interface IOr et ICuir (Nos deux héros possèdent une quantité d'Or et de Cuir, au départ 0 elles vont augmenter à chaque monstre tué)
    public abstract class Hero
    {
        //Propriétés public Cuir et Or (setter en privé)

        //Constructeur vide qui reprend le constructeur parent

        //Méthode SeReposer, ne renvoie rien, pas de param
        //Appel de la méthode dans Personnage qui remet les PV au max

        //Méthode Dépouiller, ne renvoie rien, prend en paramètre un Monstre
        //Si le monstre est IOr
        //On ajoute l'or du monstre à l'or du héro
        //Si le monstre est ICuir
        //On ajoute le cuir du monstre au cuir du héro
        //Pas de ifelse puisque notre monstre peut être Ior et Icuir, et dans ce cas là on voudra passer dans les deux if :)
    }
}
