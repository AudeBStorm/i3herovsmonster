﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroesMonstersInterface3.Models
{
    //classe abstraite : on ne veut pas pouvoir faire de nouvelle instance d'un personnage, elle sert seulement pour l'héritage
    public abstract class Personnage
    {
        //Action Meurt prenant en paramètre un personnage

        //Propriétés entières en virtual : End, For
        //Propriété entière en public : PV (dans le set, si PV passe à 0 ou mois, on déclenche l'action Meurt)

        //Constructeur du personnage (sans param)
        //Dedans, on initialise la For et l'Endurance avec la méthode static CalculCarac de Dé
        //On met les PV au max (soit directement ici mais il faudra le refaire dans repos, soit on fait une méthode pour ça

        //Méthode protected ResetPV sans retour
        //Remet les pv au max -> PV = Endurance + Modificateur basé sur l'endurance (il nous faut donc une méthode pour obtenir la valeur du modificateur)

        //Méthode GetModif, retour en int (le modificateur) et prend en paramètre la caractéristique à évaluer
        //Si carac < 5, modif = -1
        //Si carac < 10, modif = 0
        //Si carac < 15, modif = 1
        //Sinon modif = 2

        //Méthode Frapper, pas de retour, prend en paramètre un autre personnage
        //On calcule les dégats -> Un dé 4 + Modificateur(For) 
        //P'tit console pour afficher les dégats infligés au personnage
        //On baisse les PV du personnage frappé
    }
}
